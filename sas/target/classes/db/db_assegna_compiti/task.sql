
CREATE TABLE Tasks (
    id              INT             PRIMARY KEY     AUTO_INCREMENT,
    event           INT             NOT NULL,
    recipe          INT             NOT NULL,
    shift           INT             NULL            DEFAULT NULL,
    quantity        VARCHAR(128)    NULL            DEFAULT NULL,
    estimate_time   VARCHAR(128)    NULL            DEFAULT NULL,
    is_complete     BOOLEAN         NOT NULL        DEFAULT FALSE,

    FOREIGN KEY (event)     REFERENCES Events(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,

    FOREIGN KEY (recipe)    REFERENCES Recipes(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,

    FOREIGN KEY (shift)     REFERENCES Shifts(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;