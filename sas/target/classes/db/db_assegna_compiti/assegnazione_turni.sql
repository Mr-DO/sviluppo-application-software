
CREATE TABLE ShiftsAssigment (
        id      INT         PRIMARY KEY,
        shift   INT         NOT NULL,
        user    INT         NOT NULL,

        FOREIGN KEY (shift) REFERENCES Shifts(id)
            ON UPDATE CASCADE
            ON DELETE CASCADE,

        FOREIGN KEY (user) REFERENCES Users(id)
            ON UPDATE CASCADE
            ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
