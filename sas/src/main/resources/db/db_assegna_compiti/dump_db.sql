-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: sas
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Events`
--

DROP TABLE IF EXISTS `Events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL,
  `smsh` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` tinytext,
  `chef` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Events_Menus_id_fk` (`menu`),
  KEY `Events_Users_id_fk` (`chef`),
  CONSTRAINT `Events_Menus_id_fk` FOREIGN KEY (`menu`) REFERENCES `Menus` (`id`),
  CONSTRAINT `Events_Users_id_fk` FOREIGN KEY (`chef`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Events`
--

LOCK TABLES `Events` WRITE;
/*!40000 ALTER TABLE `Events` DISABLE KEYS */;
INSERT INTO `Events` VALUES (1,1,0,'First menu create. trying .......','',2,NULL),(2,2,0,'Second menu created.',NULL,2,NULL),(3,3,1,'third menu created. it is goo menu.',NULL,4,NULL);
/*!40000 ALTER TABLE `Events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MenuItems`
--

DROP TABLE IF EXISTS `MenuItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MenuItems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT '0',
  `description` tinytext,
  `recipe` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MenuItems_Menus_id_fk` (`menu`),
  CONSTRAINT `MenuItems_Menus_id_fk` FOREIGN KEY (`menu`) REFERENCES `Menus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MenuItems`
--

LOCK TABLES `MenuItems` WRITE;
/*!40000 ALTER TABLE `MenuItems` DISABLE KEYS */;
INSERT INTO `MenuItems` VALUES (1,3,1,'Voce 1',1,0),(2,3,1,'Voce 2',1,1),(3,3,2,'Voce 3',1,0),(4,3,2,'Voce 4',1,2),(5,3,0,'Voce 0',1,0),(6,3,1,'Voce 1.5',1,2),(7,3,2,'Voce 3.5',1,1),(8,3,0,'Voce 0.2',1,1),(9,3,0,'Voce 0.8',1,2),(10,3,3,'Voce 5',1,0),(11,3,3,'Voce 6',1,1),(12,3,3,'Voce 7',1,2),(13,7,4,'nessuno testo',9,0),(14,7,0,'Agnolotti al sugo d\'arrosto',13,0),(15,7,0,'Risotto alla Milanese',5,1);
/*!40000 ALTER TABLE `MenuItems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Menus`
--

DROP TABLE IF EXISTS `Menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `menuowner` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `fingerFood` tinyint(1) DEFAULT NULL,
  `cookRequired` tinyint(1) DEFAULT NULL,
  `hotDishes` tinyint(1) DEFAULT NULL,
  `kitchenRequired` tinyint(1) DEFAULT NULL,
  `buffet` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Menus_Users_id_fk` (`menuowner`),
  CONSTRAINT `Menus_Users_id_fk` FOREIGN KEY (`menuowner`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Menus`
--

LOCK TABLES `Menus` WRITE;
/*!40000 ALTER TABLE `Menus` DISABLE KEYS */;
INSERT INTO `Menus` VALUES (1,'prova in uso',3,1,1,0,0,0,1),(2,'prova non in uso',2,0,0,1,1,1,0),(3,'prova struttura',3,0,0,0,0,0,1),(4,'Ciao',3,0,0,0,0,0,0),(7,'prova struttura',3,0,0,0,0,0,1),(8,'menuanna',4,0,0,0,0,0,0);
/*!40000 ALTER TABLE `Menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recipes`
--

DROP TABLE IF EXISTS `Recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recipes`
--

LOCK TABLES `Recipes` WRITE;
/*!40000 ALTER TABLE `Recipes` DISABLE KEYS */;
INSERT INTO `Recipes` VALUES (1,'Salsa Tonnata','p'),(2,'Vitello Tonnato','r'),(3,'Vitello Tonnato all\'Antica','r'),(4,'Brodo di Manzo Ristretto','p'),(5,'Risotto alla Milanese','r'),(6,'Pesto Ligure','p'),(7,'Trofie avvantaggiate al pesto','r'),(8,'Orata al forno con olive','r'),(9,'Insalata russa','r'),(10,'Bagnet vert','p'),(11,'Acciughe al verde','r'),(12,'Agnolotti del plin','p'),(13,'Agnolotti al sugo d\'arrosto','r'),(14,'Agnolotti burro e salvia','r'),(15,'Brasato al barolo','r'),(16,'Panna cotta','r'),(17,'Tarte tatin','r');
/*!40000 ALTER TABLE `Recipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles` (
  `id` varchar(1) NOT NULL,
  `role` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` VALUES ('c','Cuoco'),('h','Chef'),('o','Organizzatore'),('s','Servizio');
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sections`
--

DROP TABLE IF EXISTS `Sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sections` (
  `menu` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Sections_Menu_id_fk` (`menu`),
  CONSTRAINT `Sections_Menus_id_fk` FOREIGN KEY (`menu`) REFERENCES `Menus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sections`
--

LOCK TABLES `Sections` WRITE;
/*!40000 ALTER TABLE `Sections` DISABLE KEYS */;
INSERT INTO `Sections` VALUES (3,1,'Primi',1),(3,2,'Secondi',2),(3,3,'Dessert',0),(7,4,'sezioneViola',0),(8,5,'sez',0);
/*!40000 ALTER TABLE `Sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Shifts`
--

DROP TABLE IF EXISTS `Shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `hour` varchar(25) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='turni di lavoro';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Shifts`
--

LOCK TABLES `Shifts` WRITE;
/*!40000 ALTER TABLE `Shifts` DISABLE KEYS */;
INSERT INTO `Shifts` VALUES (1,'2019-06-29','09H - 10H','morging'),(2,'2019-06-28','10H - 11H','morning'),(3,'2019-07-09','14H - 16H','afternoon'),(4,'2019-07-09','20H - 21H','evening'),(5,'2019-07-30','21H - 22H','evening');
/*!40000 ALTER TABLE `Shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Table_shifts`
--

DROP TABLE IF EXISTS `Table_shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Table_shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shift` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `table_shifts_Shifts_id_fk` (`shift`),
  KEY `table_shifts_Users_id_fk` (`user`),
  CONSTRAINT `table_shifts_Shifts_id_fk` FOREIGN KEY (`shift`) REFERENCES `Shifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `table_shifts_Users_id_fk` FOREIGN KEY (`user`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabellone dei turni';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Table_shifts`
--

LOCK TABLES `Table_shifts` WRITE;
/*!40000 ALTER TABLE `Table_shifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Table_shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tasks`
--

DROP TABLE IF EXISTS `Tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `recipe` int(11) NOT NULL,
  `shift` int(11) DEFAULT NULL,
  `quantity` varchar(225) DEFAULT NULL,
  `estimate_time` varchar(225) DEFAULT NULL,
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `event` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Tasks_Events_id_fk` (`event`),
  KEY `Tasks_Recipes_id_fk` (`recipe`),
  KEY `Tasks_Shifts_id_fk` (`shift`),
  KEY `Tasks_Users_id_fk` (`user`),
  CONSTRAINT `Tasks_Events_id_fk` FOREIGN KEY (`event`) REFERENCES `Events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Tasks_Recipes_id_fk` FOREIGN KEY (`recipe`) REFERENCES `Recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1 COMMENT='i compiti ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tasks`
--

LOCK TABLES `Tasks` WRITE;
/*!40000 ALTER TABLE `Tasks` DISABLE KEYS */;
INSERT INTO `Tasks` VALUES (18,NULL,1,NULL,'','hbhjhb',1,1,3),(24,4,3,3,'kjdkdsb','hbcbhhjqbcu',1,6,3),(25,NULL,6,NULL,'nvinvidf','',0,7,3);
/*!40000 ALTER TABLE `Tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserRoles`
--

DROP TABLE IF EXISTS `UserRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRoles` (
  `user` int(11) NOT NULL,
  `role` varchar(1) NOT NULL,
  KEY `UserRoles_Roles_id_fk` (`role`),
  KEY `UserRoles_Users_id_fk` (`user`),
  CONSTRAINT `UserRoles_Roles_id_fk` FOREIGN KEY (`role`) REFERENCES `Roles` (`id`),
  CONSTRAINT `UserRoles_Users_id_fk` FOREIGN KEY (`user`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserRoles`
--

LOCK TABLES `UserRoles` WRITE;
/*!40000 ALTER TABLE `UserRoles` DISABLE KEYS */;
INSERT INTO `UserRoles` VALUES (1,'o'),(2,'h'),(2,'c'),(3,'h'),(4,'o'),(4,'h'),(5,'c');
/*!40000 ALTER TABLE `UserRoles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Marco'),(2,'Tony'),(3,'Viola'),(4,'Anna'),(5,'Giovanni');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-27 15:35:32
