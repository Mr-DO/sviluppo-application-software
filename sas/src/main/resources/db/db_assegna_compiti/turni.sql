
CREATE TABLE Shifts (
    id      INT                 PRIMARY KEY     AUTO_INCREMENT,
    date    DATETIME            NOT NULL,
    length  TINYINT UNSIGNED    NOT NULL    DEFAULT 1
    #description tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;