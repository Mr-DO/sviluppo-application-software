package it.unito.sas.businesslogic.summarySheet;

import it.unito.sas.businesslogic.manager.CateringAppManager;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Shift {

    private Date date;
    private String hour;
    private String description;

    @Setter
    private List<String> items;

    public Shift(Date date, String hour, String description){
        this.date = date;
        this.hour = hour;
        this.description = description;
        this.items = new ArrayList<>();
    }

    public String toString(){
        return this.date.toString() + " => " + this.hour;
    }

}
