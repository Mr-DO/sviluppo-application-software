package it.unito.sas.businesslogic;

import it.unito.sas.businesslogic.menu.Menu;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import lombok.Getter;
import lombok.Setter;

import java.time.chrono.ChronoLocalDateTime;
import java.util.Date;

public class Event {

    @Getter
    @Setter
    private User chef;
    @Getter
    @Setter
    private Menu menu;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private Date date;
    @Getter
    @Setter
    private SummarySheet shsm;

    public Event(User chef, String name) {
        this.name = name;
        this.chef = chef;
    }


    public String toString() {
        return this.name + (this.date == null ? "" : " | in data: " + this.date.toString()) + (this.chef == null ? "" : " | Chef: " + this.chef.toString());
    }


    public boolean containsSummarySheet() {
        return this.shsm != null;
    }

}
