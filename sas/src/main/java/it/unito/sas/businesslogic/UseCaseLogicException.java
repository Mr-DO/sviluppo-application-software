package it.unito.sas.businesslogic;

public class UseCaseLogicException extends RuntimeException {
    public UseCaseLogicException(String msg) {
        super(msg);
    }
}
