package it.unito.sas.businesslogic;

import java.util.HashSet;
import java.util.Set;
import static it.unito.sas.businesslogic.User.Role.*;


public class User {
    public enum Role {Cuoco, Chef, Organizzatore, Servizio}

    private String name;
    private Set<Role> roles; // le prof hanno scelto che un impiegato puo avere piu ruoli

    public User(String name) {
        this.name = name;
        this.roles = new HashSet<Role>();
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    private boolean is(Role role) {
        return this.roles.contains(role);
    }

    public boolean isChef() {
        return is(Chef);
    }

    public boolean isCook(){
        return is(Cuoco);
    }

    public String toString() {
        return this.name;
    }

    public String getName() {
        return name;
    }
}
