package it.unito.sas.businesslogic.summarySheet;


import it.unito.sas.businesslogic.Recipe;
import it.unito.sas.businesslogic.User;
import it.unito.sas.businesslogic.menu.Menu;
import it.unito.sas.businesslogic.menu.MenuItem;

import java.util.List;
import java.util.stream.Collectors;


public class SummarySheet{

    private List<Task> tasks;
    private int swp1 = -1;
    private int swp2 = -1;

    public SummarySheet(List<Task> tasks){
        this.tasks = tasks;
    }

    public SummarySheet(Menu menu){
        this.tasks = menu.getAllItem().stream()
                .map(MenuItem::getRecipe)
                .map(Task::new)
                .collect(Collectors.toList());
    }


    public List<Task> getTasks() {
        return tasks;
    }

    public Task addTask(Recipe recipe, User cook, Shift shift, String quantity, String estimateTime, boolean complete){
        Task task = new Task(recipe, cook, shift, quantity, estimateTime, complete);
        this.tasks.add(task);
        return task;
    }

    public void modifyTask(Task task, Recipe recipe, User cook, Shift shift, String quantity, String estimateTime, boolean complete){
        task.setRecipe(recipe);
        task.setUser(cook);
        task.setShift(shift);
        task.setQuantity(quantity);
        task.setEstimatedTime(estimateTime);
        task.setComplete(complete);
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void setSwp1(int swp1) { this.swp1 = swp1; }

    public void setSwp2(int swp2) { this.swp2 = swp2; }

    public int getSwp1() { return swp1; }

    public int getSwp2() { return swp2; }
}
