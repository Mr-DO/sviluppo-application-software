package it.unito.sas.businesslogic.menu;

import it.unito.sas.businesslogic.Event;
import it.unito.sas.businesslogic.event_receiver.MenuEventReceiver;
import it.unito.sas.businesslogic.event_receiver.SummarySheetEventReceiver;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import it.unito.sas.businesslogic.summarySheet.Task;

public class BaseEventReceiver implements MenuEventReceiver, SummarySheetEventReceiver {
    @Override
    public void notifyMenuCreated(Menu m) {
    }

    @Override
    public void notifySectionAdded(Menu m, Section s) {

    }

    @Override
    public void notifyItemAdded(Menu m, Section s, MenuItem it) {

    }

    @Override
    public void notifyMenuPublished(Menu m) {

    }

    @Override
    public void notifyMenuDeleted(Menu m) {

    }

    @Override
    public void notifySectionRemoved(Menu m, Section s) {

    }

    @Override
    public void notifySectionNameChanged(Menu m, Section s) {

    }

    @Override
    public void notifySectionsRearranged(Menu m) {

    }

    @Override
    public void notifyItemsRearranged(Menu m, Section s) {

    }

    @Override
    public void notifyItemsRearrangedInMenu(Menu m) {

    }

    @Override
    public void notifyItemMoved(Menu m, Section oldS, Section newS, MenuItem it) {

    }

    @Override
    public void notifyItemDescriptionChanged(Menu m, MenuItem it) {

    }

    @Override
    public void notifyItemDeleted(Menu m, MenuItem it) {

    }

    @Override
    public void notifyMenuTitleChanged(Menu m) {

    }

    @Override
    public void notifySummarySheetCreated(Event event, SummarySheet sm) {

    }

    @Override
    public void notifySummarySheetDeleted(Event event, SummarySheet sm) {

    }

    @Override
    public void notifyTaskAdded(Event event, SummarySheet sm, Task task) {

    }

    @Override
    public void notifyTaskRemoved(SummarySheet sm, Task task) {

    }

    @Override
    public void notifyTaskChanged(Task task) {

    }

    @Override
    public void notifyTaskRearraged(SummarySheet sm) {

    }
}
