package it.unito.sas.businesslogic.manager;

import it.unito.sas.businesslogic.summarySheet.Shift;

import java.util.List;
import java.util.stream.Collectors;

public class ShiftManager {

    private List<Shift> allShift = null;

    public void initialize(){}

    public List<Shift> getAllShift() {
        if (allShift == null) allShift = CateringAppManager.dataManager.loadShifts();
        return allShift;
    }

    public List<String> getItems() {
        return CateringAppManager.dataManager.loadTasks()
                .stream()
                .filter(task -> task.getUser() != null || task.getShift() != null)
                .map(task -> (task.getUser() == null ? "Cuoco non ancora assegnato" : "Cuoco assegnato => " + task.getUser().toString()) + " | " +
                        (task.getShift() == null ? "Turno non definito" : "Turno => " + task.getShift()) + " | " +
                        (task.getRecipe() == null ? "Compito non ancora assegnato" : "Compito => " + task.getRecipe()))
                .collect(Collectors.toList());
    }
}
