package it.unito.sas.businesslogic.manager;

import it.unito.sas.businesslogic.User;

import java.util.List;

public class UserManager {

    private User currentUser;
    private List<User> allCook = null;

    public void initialize() {
        // Questa è una versione "mockup" di UserManager
        // Ossia una versione semplificata che ha lo scopo di testare l'applicazione
        // Per questa ragione il metodo initialize() carica un utente di default dal DB
        this.currentUser = CateringAppManager.dataManager.loadUser("Anna");
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public List<User> getAllCook(){
        if(allCook == null) allCook = CateringAppManager.dataManager.loadCooks();
        return allCook;
    }
}
