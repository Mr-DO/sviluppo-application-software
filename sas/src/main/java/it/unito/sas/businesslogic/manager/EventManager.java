package it.unito.sas.businesslogic.manager;

import it.unito.sas.businesslogic.Event;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class EventManager {

    @Getter @Setter private Event currentEvent;
    private List<Event> allEvents;

    public void initialize() {}


    public List<Event> getAllEvents() {
        if (allEvents == null) {
            allEvents = new ArrayList<>(CateringAppManager.dataManager.loadEvents());
        }

        return allEvents;
    }

}
