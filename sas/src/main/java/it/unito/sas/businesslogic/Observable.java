package it.unito.sas.businesslogic;

import it.unito.sas.businesslogic.event_receiver.EventReceiver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public abstract class Observable<E extends EventReceiver> {

    protected List<E> receivers;

    protected Observable(){
        receivers = new ArrayList<>();
    }

    protected void notifyReceivers(Consumer<E> c){
        receivers.forEach(c);
    }

    public void registerAll(Collection<E> c){
        receivers.addAll(c);
    }
}
