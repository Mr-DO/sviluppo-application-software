package it.unito.sas.businesslogic.event_receiver;

import it.unito.sas.businesslogic.Event;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import it.unito.sas.businesslogic.summarySheet.Task;

public interface SummarySheetEventReceiver extends EventReceiver {

    void notifySummarySheetCreated(Event event, SummarySheet sm);
    void notifySummarySheetDeleted(Event event, SummarySheet sm);

    void notifyTaskAdded(Event event, SummarySheet sm, Task task);
    void notifyTaskRemoved(SummarySheet sm, Task task);
    void notifyTaskChanged(Task task);
    void notifyTaskRearraged(SummarySheet sm);
}
