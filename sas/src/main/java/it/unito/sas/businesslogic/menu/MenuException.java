package it.unito.sas.businesslogic.menu;

public class MenuException extends RuntimeException {
    public MenuException(String s) {
        super(s);
    }
}
