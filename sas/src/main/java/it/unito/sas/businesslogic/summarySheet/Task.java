package it.unito.sas.businesslogic.summarySheet;

import it.unito.sas.businesslogic.Recipe;
import it.unito.sas.businesslogic.User;
import lombok.Getter;
import lombok.Setter;

public class Task {

    @Getter @Setter private User user;
    @Getter @Setter private Recipe recipe;
    @Getter @Setter private Shift shift;
    @Getter @Setter private String quantity;
    @Getter @Setter private String estimatedTime;
    @Getter @Setter private boolean isComplete;
    @Getter @Setter private int position;

    public Task(Recipe recipe){
        this.recipe = recipe;
    }

    public Task(Recipe recipe, User user, Shift shift, String quantity, String estimatedTime, boolean isComplete){
        this(recipe);
        this.user = user;
        this.shift = shift;
        this.quantity = quantity;
        this.estimatedTime = estimatedTime;
        this.isComplete = isComplete;
    }

}
