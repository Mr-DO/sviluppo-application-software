package it.unito.sas.businesslogic.manager;

import it.unito.sas.businesslogic.Event;
import it.unito.sas.businesslogic.Observable;
import it.unito.sas.businesslogic.Recipe;
import it.unito.sas.businesslogic.User;
import it.unito.sas.businesslogic.event_receiver.SummarySheetEventReceiver;
import it.unito.sas.businesslogic.menu.BaseEventReceiver;
import it.unito.sas.businesslogic.menu.Menu;
import it.unito.sas.businesslogic.summarySheet.Shift;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import it.unito.sas.businesslogic.summarySheet.Task;

import java.util.ArrayList;
import java.util.List;

public class SummarySheetManager extends Observable<SummarySheetEventReceiver> {


    private Task currentTask;

    SummarySheetManager(){
        super();
        this.receivers.add(new BaseEventReceiver(){

        });
        System.out.println("[SummarySheetManager] currentEvent => " + CateringAppManager.eventManager.getCurrentEvent());
    }

    public void initialize(){}

    public List<Task> getAllTasks(){
        Menu currentMenu = CateringAppManager.menuManager.getCurrentMenu();
        Event currentEvent = CateringAppManager.eventManager.getCurrentEvent();

        if(currentMenu != null && currentEvent != null){
            return  currentEvent.containsSummarySheet()
                    ?   currentEvent.getShsm().getTasks()
                    :   CateringAppManager.dataManager.loadTasks(currentMenu, currentEvent);
        }
        return null;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public Task getCurrentTask() {
        return currentTask;
    }

    public void modifyTask(Task task, Recipe recipe, User cook, Shift shift, String quantity, String estimateTime, boolean complete){
        CateringAppManager.eventManager.getCurrentEvent().getShsm().modifyTask(task, recipe, cook, shift, quantity, estimateTime, complete);
        notifyReceivers(e -> e.notifyTaskChanged(task));
    }

    public Task addTask(Recipe recipe, User cook, Shift shift, String quantity, String estimateTime, boolean complete){
        List<Task> ts = this.getAllTasks();
        Task task = CateringAppManager.eventManager.getCurrentEvent().getShsm().addTask(recipe, cook, shift, quantity, estimateTime, complete);
        task.setPosition(ts.get(ts.size() -1 ) == null ? 1 : (ts.get(ts.size() -1 )).getPosition() + 1);
        notifyReceivers(e -> e.notifyTaskAdded(
                CateringAppManager.eventManager.getCurrentEvent(),
                CateringAppManager.eventManager.getCurrentEvent().getShsm(),
                task
        ));
        return task;
    }

    public void deleteTask(Task task){
        notifyReceivers(e -> e.notifyTaskRemoved(CateringAppManager.eventManager.getCurrentEvent().getShsm(), task));
    }

    public void taskRearranged(){
        notifyReceivers(e -> e.notifyTaskRearraged(CateringAppManager.eventManager.getCurrentEvent().getShsm()));
    }

    public void addReceiver(SummarySheetEventReceiver receiver){
        this.receivers.add(receiver);
    }

    public void generateSummarySheet(Event event){
        event.setShsm(event.getMenu() == null ? new SummarySheet(new ArrayList<>()) : new SummarySheet(event.getMenu()));
        notifyReceivers(e -> e.notifySummarySheetCreated(event, event.getShsm()));
    }

    public void deleteSummarySheet(Event event){
        SummarySheet sm = event.getShsm();
        event.setShsm(null);
        notifyReceivers(r -> r.notifySummarySheetDeleted(event, sm));
    }

}
