package it.unito.sas.ui.menu;

import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.ui.summarysheet.ParentController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class MainController extends ParentController {

    @FXML
    private BorderPane mainPane;

    @FXML
    private Label userName;


    @FXML
    public void initialize() {
        userName.setText(CateringAppManager.userManager.getCurrentUser().toString());

        try {

            FXMLLoader menuListLoader = new FXMLLoader(getClass().getResource("/fxml/menulist.fxml"));
            Parent menuList = menuListLoader.load();
            ParentController menuListController = menuListLoader.getController();
            menuListController.setParentController(this);
            mainPane.setCenter(menuList);

        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }


}
