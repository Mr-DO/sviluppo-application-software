package it.unito.sas.ui.summarysheet;

import it.unito.sas.businesslogic.Recipe;
import it.unito.sas.businesslogic.User;
import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.businesslogic.summarySheet.Shift;
import it.unito.sas.businesslogic.summarySheet.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import java.util.List;

public class TaskEditController {

    private Task curentTask;

    @FXML
    private BorderPane taskedit;

    @FXML
    private Button buttonSave;

    @FXML
    private Button buttonApply;

    @FXML
    private Button buttonModify;

    @FXML
    private Label labelCuoco;

    @FXML
    private ComboBox<User> comboBoxCuoco;
    private ObservableList<User> listCuoco;

    @FXML
    private ComboBox<Recipe> comboBoxRecipe;
    private ObservableList<Recipe> listRecipe;

    @FXML
    private ComboBox<Shift> comboBoxTurni;
    private ObservableList<Shift> listShift;

    @FXML
    private TextArea textAreaPorzioni;

    @FXML
    private TextArea textAreaTempoStimato;

    @FXML
    private CheckBox checkBoxCompletato;


    public void setUp(){
        this.curentTask = CateringAppManager.summarySheetManager.getCurrentTask();

        if(this.curentTask != null) {
            this.reset();
            labelCuoco.setText(this.curentTask.getUser() == null ? "cuoco non assegnato!" : this.curentTask.getUser().toString());

            listCuoco = FXCollections.observableList(CateringAppManager.userManager.getAllCook());
            comboBoxCuoco.setItems(listCuoco);
            if (this.curentTask.getUser() != null && listCuoco.size() > 0) {
                if (listCuoco.get(0) != null) listCuoco.add(0, null);
                comboBoxCuoco.setValue(this.curentTask.getUser());
            }

            listShift = FXCollections.observableList(CateringAppManager.shiftManager.getAllShift());
            comboBoxTurni.setItems(listShift);
            if (this.curentTask.getShift() != null && listShift.size() > 0) {
                if (listShift.get(0) != null) listShift.add(0, null);
                comboBoxTurni.setValue(this.curentTask.getShift());
            }

            listRecipe = FXCollections.observableList(CateringAppManager.recipeManager.getRecipes());
            comboBoxRecipe.setItems(listRecipe);
            if (this.curentTask.getRecipe() != null && listRecipe.size() > 0) {
                if (listRecipe.get(0) != null) listRecipe.add(0, null);
                comboBoxRecipe.setValue(this.curentTask.getRecipe());
            }

            textAreaPorzioni.setText(this.curentTask.getQuantity() == null ? "" : this.curentTask.getQuantity());
            textAreaTempoStimato.setText(this.curentTask.getEstimatedTime() == null ? "" : this.curentTask.getEstimatedTime());
            checkBoxCompletato.setSelected(this.curentTask.isComplete());
        }

        this.buttonSave.setDisable(true);
        this.buttonApply.setDisable(false);
    }

    public void setVisibility(boolean visibility){
        taskedit.setVisible(visibility);
    }

    public void buttonApplyAction(){
            User cook = comboBoxCuoco.getSelectionModel().getSelectedItem();
            Recipe recipe = comboBoxRecipe.getSelectionModel().getSelectedItem();
            Shift shift = comboBoxTurni.getSelectionModel().getSelectedItem();
            String quantity = textAreaPorzioni.getText();
            String estimatedTime = textAreaTempoStimato.getText();
            boolean complete = checkBoxCompletato.isSelected();
            CateringAppManager.summarySheetManager.modifyTask(curentTask, recipe, cook, shift, quantity, estimatedTime, complete);
            this.vewTask(true);
    }

    public void buttonSaveAction(){
        System.out.println("do nothing now");
        if(comboBoxRecipe.getSelectionModel().getSelectedItem() == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alert Informazione Crea Compiti!");
            alert.setHeaderText("Creazione di Compiti");
            alert.setContentText("per creare un compito devi almeno selzionare un compito!");
            alert.showAndWait();
        }
        else{
            Recipe recipe = comboBoxRecipe.getSelectionModel().getSelectedItem();
            User cook = comboBoxCuoco.getSelectionModel().getSelectedItem();
            Shift shift = comboBoxTurni.getSelectionModel().getSelectedItem();
            String quantity = textAreaPorzioni.getText();
            String estimateTime = textAreaTempoStimato.getText();
            boolean complete = checkBoxCompletato.isSelected();
            CateringAppManager.summarySheetManager.addTask(recipe, cook, shift, quantity, estimateTime, complete);
        }
    }

    public void reset(){
        labelCuoco.setText("Cuoco");
        listCuoco = FXCollections.observableList(CateringAppManager.userManager.getAllCook());
        comboBoxCuoco.setItems(listCuoco);
        comboBoxCuoco.setValue(null);
        listShift = FXCollections.observableList(CateringAppManager.shiftManager.getAllShift());
        comboBoxTurni.setItems(listShift);
        comboBoxTurni.setValue(null);
        listRecipe = FXCollections.observableList(CateringAppManager.recipeManager.getRecipes());
        comboBoxRecipe.setItems(listRecipe);
        comboBoxRecipe.setValue(null);
        textAreaPorzioni.setText("");
        textAreaTempoStimato.setText("");
        checkBoxCompletato.setSelected(false);
    }

    public void beforeAdd(){
        this.reset();
        this.buttonApply.setDisable(true);
        this.buttonSave.setDisable(false);
        this.setVisibility(true);
        this.vewTask(false);
        this.buttonApply.setDisable(true);
    }

    public void vewTask(boolean toggle){
        comboBoxCuoco.setDisable(toggle);
        comboBoxRecipe.setDisable(toggle);
        comboBoxTurni.setDisable(toggle);
        textAreaPorzioni.setDisable(toggle);
        textAreaTempoStimato.setDisable(toggle);
        checkBoxCompletato.setDisable(toggle);
        this.buttonModify.setDisable(!toggle);
        this.buttonApply.setDisable(toggle);
    }

    public void buttonModifyAction(){
        this.vewTask(false);
    }
}
