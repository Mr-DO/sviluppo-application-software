package it.unito.sas.ui.summarysheet;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SplitPane;

import java.io.IOException;

public class SmShTaskEditController extends ParentController{

    private TaskEditController taskEditController;
    private SummarySheetListController summarySheetListController;

    @FXML
    private SplitPane splitPaneContainer;


    @FXML
    public void initialize(){
        try {

            FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/fxml/taskedit.fxml"));
            loader1.load();
            taskEditController = loader1.getController();

            FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/summarysheetlist.fxml"));
            loader2.load();
            summarySheetListController = loader2.getController();
            summarySheetListController.setParentController(this);

            splitPaneContainer.getItems().set(0,loader2.getRoot());
            splitPaneContainer.getItems().set(1, loader1.getRoot());

        }catch (IOException exc) {
            exc.printStackTrace();
        }

    }


    public void setUp(){
        summarySheetListController.setUp(taskEditController);
        taskEditController.setVisibility(false);
        //taskEditController.setUp();
    }

}
