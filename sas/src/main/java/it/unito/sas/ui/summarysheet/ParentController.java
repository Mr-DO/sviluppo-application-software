package it.unito.sas.ui.summarysheet;

public abstract class ParentController {

    public ParentController parentController;

    public void setParentController(ParentController parentController) {
        this.parentController = parentController;
    }

    public void setPrincipalScene(Object o) {
        parentController.setPrincipalScene(null);
    }

}
