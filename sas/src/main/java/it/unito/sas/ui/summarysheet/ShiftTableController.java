package it.unito.sas.ui.summarysheet;

import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.businesslogic.summarySheet.Shift;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

public class ShiftTableController {

    @FXML
    private ListView<String> shiftList;

    @FXML
    public void initialize(){
        ObservableList<String> listShift = FXCollections.observableList(CateringAppManager.shiftManager.getItems());
        shiftList.setItems(listShift);
        shiftList.setPrefWidth(780);
        shiftList.setPrefHeight(400);
    }
}
