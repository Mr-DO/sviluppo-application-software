package it.unito.sas.ui.summarysheet;


import it.unito.sas.businesslogic.Event;
import it.unito.sas.businesslogic.event_receiver.SummarySheetEventReceiver;
import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.businesslogic.menu.BaseEventReceiver;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import it.unito.sas.businesslogic.summarySheet.Task;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SummarySheetListController extends ParentController{

    private ObservableList<Task> taskObservableList;
    private List<Task> tasks = new ArrayList<>();
    private Task taskSelected;
    private TaskEditController taskEditController;
    private List<SummarySheetEventReceiver> receiver = new ArrayList<>();

    @FXML
    private ListView<Task> listTask;

    @FXML
    private Button addButton;

    @FXML
    private Button rmButton;

    @FXML
    private Button upButton;

    @FXML
    private Button downButton;


    @FXML
    private void initialize(){
        this.addReceivers(new BaseEventReceiver(){
            @Override
            public void notifyTaskAdded(Event event, SummarySheet sm, Task task) {
                resetTaskList();
            }

            @Override
            public void notifyTaskRemoved(SummarySheet sm, Task task) {
                resetTaskList();
            }

            @Override
            public void notifyTaskChanged(Task task) {
                resetTaskList();
            }

            @Override
            public void notifyTaskRearraged(SummarySheet sm) { resetTaskList(); }
        });

        listTask.setCellFactory(lw -> new TaskCell());

    }

    @Override
    public void setPrincipalScene(Object o) {

    }

    @FXML
    public void selectBackAction(){
        parentController.setPrincipalScene(null);
    }

    @FXML
    public void selectShiftAction(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("TURNI");
        alert.setHeaderText("");
        alert.setWidth(800);
        FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/fxml/shiftList.fxml"));
        try {
            Parent main = mainLoader.load();
            alert.setGraphic(main);

        }catch (IOException e){
              e.printStackTrace();
        }

        alert.showAndWait();
    }

    private class TaskCell extends ListCell<Task> {

        public void updateItem(Task item, boolean empty){
            super.updateItem(item, empty);

            setText(empty ? "" : item.getRecipe().toString());

            if(item != null){
                setTextFill(item.isComplete() ? Color.GREEN : Color.BLACK);
            }
        }
    }

    public void setUp(TaskEditController taskEditController){
        this.taskEditController = taskEditController;
        this.resetTaskList();
        CateringAppManager.summarySheetManager.registerAll(this.receiver);
    }

    public void resetTaskList(){
        tasks = CateringAppManager.summarySheetManager.getAllTasks();
        if(tasks != null) {
            taskObservableList = FXCollections.observableList(tasks);
            listTask.setItems(taskObservableList);
            listTask.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
                taskSelected = listTask.getSelectionModel().getSelectedItem();
                this.onItemListVew();
            });
            listTask.setOnMouseClicked(event -> {
                taskSelected = listTask.getSelectionModel().getSelectedItem();
                if(taskSelected != null){
                    this.onItemListVew();
                }
            });
        }
    }

    public void addButtonTask(){
        this.taskEditController.beforeAdd();
    }

    public void downButtonAction(){
        int s = tasks.indexOf(this.taskSelected);
        this.swap(this.taskSelected, tasks.get(s+1));
    }

    public void upButtonAction(){
        int s = tasks.indexOf(this.taskSelected);
        this.swap(this.taskSelected, tasks.get(s-1));
    }

    private void onItemListVew(){
        CateringAppManager.summarySheetManager.setCurrentTask(taskSelected);
        taskEditController.setUp();
        taskEditController.setVisibility(true);
        taskEditController.vewTask(true);
        rmButton.setDisable(false);
        this.upButton.disableProperty().bind(new SimpleBooleanProperty(!(taskSelected != null && tasks.size() > 0 && !taskSelected.equals(tasks.get(0)))));
        this.downButton.disableProperty().bind(new SimpleBooleanProperty(!(taskSelected != null && tasks.size() > 0 && !taskSelected.equals(tasks.get(tasks.size() - 1)))));
    }

    public void rmButtonTask(){
        CateringAppManager.summarySheetManager.deleteTask(taskSelected);
        this.rmButton.setDisable(true);
        taskEditController.setVisibility(false);
    }

    private void  addReceivers(SummarySheetEventReceiver receiver){
        this.receiver.add(receiver);
    }

    private void swap(Task task1, Task task2) {
        int tmp = task1.getPosition();
        task1.setPosition(task2.getPosition());
        task2.setPosition(tmp);
        int t1 = this.tasks.indexOf(task1);
        int t2 = this.tasks.indexOf(task2);
        this.tasks.set(t1, task2);
        this.tasks.set(t2, task1);

        var currentSummarySheet = CateringAppManager.eventManager.getCurrentEvent().getShsm();
        currentSummarySheet.setSwp1(t1);
        currentSummarySheet.setSwp2(t2);
        CateringAppManager.summarySheetManager.taskRearranged();
    }
}
