package it.unito.sas.ui.menu;

public interface MenuEditListener {
     void onClose(boolean publish);
}
