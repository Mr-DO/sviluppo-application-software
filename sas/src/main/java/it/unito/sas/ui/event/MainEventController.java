package it.unito.sas.ui.event;

import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.ui.summarysheet.ParentController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class MainEventController extends ParentController {

    @FXML
    private BorderPane mainEventPane;

    @FXML
    private Label userEventName;

    Parent eventList;

    @FXML
    public void initialize() {
        userEventName.setText(CateringAppManager.userManager.getCurrentUser().toString());

        try {

            FXMLLoader eventListLoader = new FXMLLoader(getClass().getResource("/fxml/eventList.fxml"));
            eventList = eventListLoader.load();
            ParentController eventListController = eventListLoader.getController();
            eventListController.setParentController(this);

            mainEventPane.setCenter(eventList);

        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }

    @Override
    public void setPrincipalScene(Object o) {

        if(o == null) {
            if (parentController != null)
                parentController.setPrincipalScene(null);
        }else {
            this.initialize();
        }
    }

}
