package it.unito.sas.ui.event;

import it.unito.sas.businesslogic.Event;
import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.businesslogic.menu.BaseEventReceiver;
import it.unito.sas.businesslogic.summarySheet.SummarySheet;
import it.unito.sas.ui.summarysheet.ParentController;
import it.unito.sas.ui.summarysheet.SmShTaskEditController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.io.IOException;

public class EventListController extends ParentController {

    private Event selectedEvent;
    private SmShTaskEditController smShTaskEditController;

    @FXML
    private ListView<Event> eventList;

    @FXML
    private BorderPane eventListContainer;

    @FXML
    private SplitPane smShTaskEdit;

    @FXML
    private Button selectEventButton;

    @FXML
    private Button generateButton;

    @FXML
    private Button deleteButton;


    @FXML
    public void initialize() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/smShTaskEdit.fxml"));
            smShTaskEdit = loader.load();
            smShTaskEditController = loader.getController();
            smShTaskEditController.setParentController(this);

        } catch (IOException exc) {
            exc.printStackTrace();
        }

        eventList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.resetEventList();

        eventList.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
            selectedEvent = eventList.getSelectionModel().getSelectedItem();
            boolean ownershipOk = selectedEvent != null && (selectedEvent.getChef().equals(CateringAppManager.userManager.getCurrentUser()));
            boolean editable    = selectedEvent != null &&  selectedEvent.getChef().isChef();
            boolean containSummarySheet = selectedEvent != null && selectedEvent.containsSummarySheet();
            selectEventButton.setDisable(selectedEvent == null);
            selectEventButton.setDisable(!ownershipOk || !editable || !containSummarySheet);
            generateButton.setVisible(ownershipOk && !selectedEvent.containsSummarySheet());
            deleteButton.setVisible(ownershipOk && selectedEvent.containsSummarySheet());
        });

        CateringAppManager.summarySheetManager.addReceiver(new BaseEventReceiver(){
            @Override
            public void notifySummarySheetCreated(Event event, SummarySheet sm){
                eventList.refresh();
            }

            @Override
            public void notifySummarySheetDeleted(Event event, SummarySheet sm){
                eventList.refresh();
            }
        });

        eventList.setCellFactory(
                lw -> new EventCell()
        );
    }

    private class EventCell extends ListCell<Event> {

        @Override
        public void updateItem(Event item, boolean empty){
            super.updateItem(item, empty);

            if(empty || item == null){
                setText(null);
                setGraphic(null);
            }else{

                Parent cellInflating = null;
                try {
                    cellInflating = FXMLLoader.load(getClass().getResource("/fxml/eventListCellNode.fxml"));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                ((Circle) cellInflating.lookup("#circle"))
                        .setFill(
                        item.containsSummarySheet() ? Color.GREEN : Color.RED
                );

                ((Label) cellInflating.lookup("#label")).setText(item.toString());
                setGraphic(cellInflating);
            }
        }
    }

    private void resetEventList() {
        var observableEvents = FXCollections.observableList(CateringAppManager.eventManager.getAllEvents());
        eventList.setItems(observableEvents);
    }

    @FXML
    private void selectEventAction(ActionEvent actionEvent){
        if (selectedEvent != null) {
            CateringAppManager.eventManager.setCurrentEvent(selectedEvent);
            System.out.println("[EventListController] currentEvent => " + CateringAppManager.eventManager.getCurrentEvent());
            CateringAppManager.menuManager.setCurrentMenu(selectedEvent.getMenu());
        }
        System.out.println(smShTaskEdit);
        eventListContainer.setCenter(smShTaskEdit);
        smShTaskEditController.setUp();
    }


    @FXML
    public void selectBackAction(){
        if(parentController != null)
            parentController.setPrincipalScene(null);
    }


    @FXML
    private void genereteSummarySheet(){
        CateringAppManager.summarySheetManager.generateSummarySheet(eventList.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void deleteSummarySheet(){
        CateringAppManager.summarySheetManager.deleteSummarySheet(eventList.getSelectionModel().getSelectedItem());
    }

    @Override
    public void setPrincipalScene(Object o) {
        parentController.setPrincipalScene(new Object());
    }

}
