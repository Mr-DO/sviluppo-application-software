package it.unito.sas;

import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.ui.summarysheet.ParentController;
import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private CateringAppManager app;

    private static Stage primaryStage;

    private static Scene mainScene;


    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;
        this.app = CateringAppManager.getInstance();


        FXMLLoader mainLoader = new FXMLLoader(Main.class.getResource("/fxml/mainScene.fxml"));
        Parent main = mainLoader.load();

        mainScene = new Scene(main);

        primaryStage.setScene(mainScene);

        //MainSceneController mainSceneController = mainLoader.getController();
        //mainSceneController.setParentController(this);

        primaryStage.setWidth(800);
        primaryStage.setHeight(500);
        // primaryStage.setMaximized(true);
        primaryStage.show();
    }


    public static Stage getStage(){

        return primaryStage;
    }


    public static void main(String[] args) {
        Application.launch(args);
    }

    public static void setPrincipalScene() {
        primaryStage.setScene(mainScene);

    }

}
