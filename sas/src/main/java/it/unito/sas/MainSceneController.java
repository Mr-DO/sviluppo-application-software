package it.unito.sas;

import it.unito.sas.businesslogic.User;
import it.unito.sas.businesslogic.manager.CateringAppManager;
import it.unito.sas.ui.event.MainEventController;
import it.unito.sas.ui.menu.MainController;
import it.unito.sas.ui.summarysheet.ParentController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class MainSceneController extends ParentController {

    @FXML
    private VBox mainScenePane;

    @FXML
    private Label userName;
    @FXML
    private Button buttonEvents;
    @FXML
    private Button buttonRecipes;
    @FXML
    private Button buttonMenu;


    private Scene mainScene = null;

    @FXML
    public void initialize() {
        userName.setText(CateringAppManager.userManager.getCurrentUser().toString());
    }

    @FXML
    private void buttonMenuPressed(ActionEvent actionEvent) {
        try {
            FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
            Parent main = mainLoader.load();
            mainScene = new Scene(main);
            ParentController mainController = mainLoader.getController();
            mainController.setParentController(this);
            Main.getStage().setScene(mainScene);

        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    @FXML
    private void buttonEventPressed(ActionEvent actionEvent) {
        User currentUser = CateringAppManager.userManager.getCurrentUser();
        if(currentUser.isChef()) {
            try {
                FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/fxml/mainEvent.fxml"));
                Parent main = mainLoader.load();
                ParentController mainEventController = mainLoader.getController();
                mainEventController.setParentController(this);
                mainScene = new Scene(main);

                Main.getStage().setScene(mainScene);

            } catch (IOException exc) {
                exc.printStackTrace();
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alert Informazione!");
            alert.setHeaderText("Area riservato");
            alert.setContentText("Area riservato ai chef cuochi!");
            alert.showAndWait();
        }
    }

    @Override
    public void setPrincipalScene(Object o) {
        Main.setPrincipalScene();
    }

}
