# Use Case "Assegnare Compiti"

---
Autori :
 * Domeneghetti Alessandro
 > aggiungete i vostri nomi
---

## Table of Content
 1.[Storie Utente](#Storie-Utente)
 2.[Estrazione passi](#Estrazione-passi)
 3.[Scrittura Operazioni]($Scrittura-operazioni)
 4.[Cockburn](#Cockburn)

### Storie utente
#### Storia 1: Chef Claudio (l’ansioso)

Quando si è deciso il menù per un evento, per prima cosa creo un foglio riepilogativo, tirando giù un elenco di tutto ciò che va fatto: le ricette che compongono il menù e altre preparazioni di base. Può anche essere che decida in corsa di aggiungere alcune cose, ad esempio pane o grissini o focaccia, non vengono scritti nel menù ma li si deve comunque preparare.
Una volta che ho l’elenco di tutto ciò che deve essere fatto, le ordino per importanza o per difficoltà Faccio lo stesso con eventuali preparazioni che, se realizzate male, potrebbero compromettere la riuscita dell’intero piatto. Guardo poi sul tabellone dei turni quando queste ricette possono essere eseguite e le assegno al cuoco più esperto disponibile in quei turni. Dal tabellone si vede cosa deve fare ciascuno e quanto ne ha da fare, così capisco se un cuoco può fare ancora qualcosa in parallelo o no.
Assegno poi le restanti ricette seguendo l’ordine che ho stilato inizialmente, sulle preparazioni meno complicate sono meno attento alla scelta del cuoco.
Segno anche sul tabellone una stima del tempo che ci metterà il cuoco a svolgere il compito che gli ho assegnato.
Alla fine degli assegnamenti revisiono tutti i turni controllando che le preparazioni siano eseguite nell’ordine giusto e rispettando le tempistiche necessarie (es. creme e dolci nell’ultimo turno, pasta che necessita di lunga lievitazione nei primi turni), perchè nel primo giro di assegnamenti  concentrandomi sulle capacità dei cuochi e sulla difficoltà delle ricette potrei aver perso di vista tempistiche e ordine di svolgimento.
Ovviamente quando assegno un compito devo dire anche la quantità o le porzioni che il cuoco deve preparare. Per alcune preparazioni talvolta si deve dividere in due, ad esempio metà la si fa un giorno e metà il giorno dopo. l’importante è che poi la quantità totale torni con quella che serve per l’evento. Oltre al tabellone dei turni c’è anche un foglio riepilogativo degli assegnamenti per un evento, queste cose – che riguardano il lavoro di più turni diversi – le si vede qui.
I cuochi vanno a segnare le cose completate sia sul tabellone del turno che su quello dell’evento, così io so sempre a che punto siamo. Chiedo anche ai cuochi di segnalare alla fine di ogni turno eventuali problematiche (tempo non sufficiente per il compito assegnato, tempi morti che hanno impedito di ottimizzare il lavoro) in modo da farne tesoro per eventuali modifiche da apportare ai tempi di esecuzione delle ricette.
Alla fine di ogni turno controllo che i cuochi abbiano svolto tutti i compiti, se qualcosa non procede per il verso giusto riorganizzo i turni successivi di conseguenza, aggiungendo ad altri turni le preparazioni non eseguite o cambiando il cuoco che ha creato il problema. 

#### Storia 2: Chef Tony (il rilassato)

Lavoro molto alla giornata. Tipicamente arrivo al mattino presto; se mi hanno affidato un nuovo evento creo il suo foglio riepilogativo dei compiti da assegnare, elencando tutte le ricette e preparazioni che lo riguardano, ma non è detto che mi occupi subito di fare le assegnazioni. 
Infatti di solito mi concentro solo sulle assegnazioni del giorno. Guardo i fogli riepilogativi di tutti gli eventi che ci aspettano da oggi a una settimana, partendo sempre da quello più vicino che ovviamente ha la priorità. 
Man mano che assegno compilo il foglio riepilogativo dell’evento segnando chi fa cosa, e quando. Lavorando in questo modo riesco anche a cumulare le preparazioni per più eventi, ad esempio se c’è da fare i grissini per due eventi diversi che avvengono domani, posso prepararli tutti insieme. Segno sui fogli riepilogativi di entrambi gli eventi l’assegnazione. Sui fogli riepilogativi ci sono anche le quantità o porzioni preparate.
Lavorando giorno per giorno posso anche sfruttare meglio eventuali preparazioni in eccesso: se ad esempio l’impasto della pizza è avanzato da ieri, mi segno sul foglio riepilogativo che non c’è bisogno che lo prepari nessuno.
Abbiamo un tabellone su cui segniamo il lavoro che viene fatto in ogni turno, potrei anche assegnare ad esempio oggi il lavoro di domani, ma è raro che lo faccia. Se però ci sono preparazioni particolarmente critiche come tempistiche, magari richiedono di essere lavorate in più turni consecutivi, segno sul calendario in che turni andranno svolte, anche senza metterci il nome di chi le farà, quello mi riservo di deciderlo quando arriva il momento. Tanto i cuochi mica hanno bisogno di sapere in anticipo cosa dovranno cucinare!


### Estrazione passi

##### Termini da aggiungere al glossario

| Termine | Descrizione |
| ------- | ----------- |
| Compito | preparazione di una certa ricetta o preparato da parte del personale addetto |
| Tabellone dei Turni | Calendario dove vengono specificati gli intervalli orari di lavoro del personale (cuochi) i loro compiti, e lo stato di essi. Quessto permette di notificare lo stato di completamento di un certo compito assegnato ad un certo cuoco |
| Tabellone dell'Evento | Calendario che permette di assegnare, e visualizzare lo stato dei compiti che spaziano su piu' turni all'interno di un evento |
| Foglio Riepilogativo dei Compiti | Insieme di tutti i compiti da assegnare |

#### Storia 1 Chef Claudio (*l’ansioso*)

1. Crea il foglio riepilogativo OPPURE(se già presente) lo recupera.
2.  Consulta il menu deciso, e OPZIONALMENTE una possibile lista delle preparazione di base.
3. Inserisce nel foglio riepilogativo un compito. 
   * Ripete *2 e 3* finché sia completato l’elenco OPPURE è soddisfatto.
4. Consulta i tabelloni dei turni e dell’evento.
5. Assegna i compiti ai diversi cuochi secondo i turni con le tempistiche, sapendo che una ricetta di un evento può essere suddivisa in più turni.
   * Ripeto *4 e 5* fino a completare tutti i compiti.  		

[^1]: Dal tabellone dei turni viene inoltre mostrato anche quanto lavoro sta correntemente eseguendo un cuoco.

#### Storia 2 Chef Tony (*il rilassato*)

1. Crea il foglio riepilogativo OPZIONALMENTE inserisce tutti i compiti sul foglio riepilogativo, OPPURE (se già presente) lo seleziona semplicemente.
2. Consulta i fogli riepilogativi dei vari eventi.
3. Assegna il compito da essere eseguito sul turno in cui dovrà essere eseguito , descrivendo cosa fare, quando (che possono esser svolti su diversi turni) e quanto(le diverse porzioni).
4. Compila il foglio riepilogativo dell’evento OPZIONALMENTE specificando anche il cuoco che dovrà essere eseguito.
   * Ripeto **3 e 4** fino a soddisfare le aspettative


### Unificazione Operazioni degli Attori

##### Possibile scelta da chiedere al committente : Tabellone dei Turni vs Tabellone dell'evento

Sembra che vi sia una sovrapposizione fra il *tabellone dell'evento* e il *tabellone dei turni*, sia il primo che il secondo permettono di assegnare compiti ai cuochi che prestano servizio in un turno di un particolare evento, e di ricevere feedback sullo stato del procedimento durante la preparazione. L'unica differenza e' che il tabellone dell'evento permette di assegnare un compito che spazia su più' turni, ma sempre all'interno dello stesso evento, dunque potrebbe essere il tabellone dei turni un caso particolare di tabellone dell'evento, i due concetti possono essere unificati oppure e' necessario mantenerli separati?

Scelta in attesa in attesa di conferma : non unificazione.

 1. Creazione del foglio riepilogativo dei compiti OPPURE selezione del precedentemente creato foglio riepilogativo dell'evento.

 2. OPZIONALMENTE consulta il menu deciso, e una possibile lista delle preparazione di base.

    \* Inserisce i compiti sul foglio riepilogativo finche gli aggrada.

 3. Consulta la tabella dell'evento selezionato OPZIONALMENTE consulta anche i fogli riepilogativi dei vari eventi.

 4. Assegna i compiti nei turni sul foglio dell'evento e OPZIONALMENTE sul foglio dei turni, specificando cosa fare, quanto(le diverse porzioni) e anche le tempistiche e OPZIONALMENTE i cuochi.
 > ripete i passi 3 e 4 finche gli aggrada.

\* Consulta il tabellone dell'evento per essere notificato sullo progresso dei singoli compiti  da parte dei cuochi.



