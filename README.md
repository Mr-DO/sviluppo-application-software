# Sviluppo Applicazioni Software (SAS) => Let go 

# Ho provato di creare delle sezioni per le diverse parte del progetto.

Dobbiamo, definire come dividere il lavoro. Secondo me iniziamo a studiare la teoria in modo da essere messi abbastanza bene, quando inizzeremmo il progetto.
Dobbiamo dcidere quando iniziare.
Proponiamo delle date.

Guardate come ho impostato le cartelle se vi va ?
Ho se dobbiamo fare in un altro modo.
Let go, we can do it. 

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Progress Working for the Course  


### Organizzazione del progetto
Ho impostato il progetto maven all'interno del nostro repository, salvado separatamente la cartella doc dove andremo a inserire tutti gli elaborati formati in ogni parte del progetto.
Ho ritenuto di non inserire direttamente questa directory dentro il progetto maven per la mole di documentazione e perche' questa non e' __"direttamente"__ legata all'implementazione. 
Se verra creata una cartella doc all'interno del progetto verra' utilizzata per contentere il javadoc.
> *Se preferite in un altro modo si puo discuterne*


##### Dunque l'organizzazione rispetta il seguente schema

* laboratorio-sas --> progetto maven
* doc --> documentazione
    * Ideazione
    * Elaborazione
    * Costruzione
    * Transizione
